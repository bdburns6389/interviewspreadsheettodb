﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToExcel;
using InterviewSpreadsheetToDB.Models;
using InterviewSpreadsheetToDB.Helpers;

namespace InterviewSpreadsheetToDB
{
    class Program
    {
        static void Main(string[] args)
        {
            //LotsExcelRead.ReadExcel();
            //ProcessingExcelRead.ReadExcel();
            //SexCodesExcelRead.ReadExcel();
            //SpecialPensExcelRead.ReadExcel();
            TreatmentsExcelRead.ReadExcel();
        }
    }
}