﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using InterviewSpreadsheetToDB.Models;
using LinqToExcel;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Helpers
{
    class TreatmentsExcelRead
    {
        private static ISessionFactory _sessionFactory;

        public static void ReadExcel()
        {
            var excel = new ExcelQueryFactory
            {
                FileName = @"C:\Users\brian\Desktop\Data_09_21_2018\wttrt.xlsx"
            };
            excel.AddMapping<TreatmentsExcel>(x => x.CurrentHospitalInDate, "Current Hospital In Date");
            excel.AddMapping<TreatmentsExcel>(x => x.DeathDate, "Death Date");
            excel.AddMapping<TreatmentsExcel>(x => x.DeathDiagnosis, "Death Diagnosis");
            excel.AddMapping<TreatmentsExcel>(x => x.DeathDiagnosisCode, "Death Diagnosis Code");
            excel.AddMapping<TreatmentsExcel>(x => x.DeathDoctor, "Death Doctor");
            excel.AddMapping<TreatmentsExcel>(x => x.DeathPenRider, "Death Pen Rider");
            excel.AddMapping<TreatmentsExcel>(x => x.DeathWeight, "Death Weight");
            excel.AddMapping<TreatmentsExcel>(x => x.DiagnosisCode, "Diagnosis Code");
            excel.AddMapping<TreatmentsExcel>(x => x.FirstHospitalInDate, "First Hospital In Date");
            excel.AddMapping<TreatmentsExcel>(x => x.ItemCode, "Item Code");
            excel.AddMapping<TreatmentsExcel>(x => x.ItemName, "Item Name");
            excel.AddMapping<TreatmentsExcel>(x => x.ItemType, "Item Type");
            excel.AddMapping<TreatmentsExcel>(x => x.LastHospitalOutDate, "Last Hospital Out Date");
            excel.AddMapping<TreatmentsExcel>(x => x.LungScore, "Lung Score");
            excel.AddMapping<TreatmentsExcel>(x => x.PenRider, "Pen Rider");
            excel.AddMapping<TreatmentsExcel>(x => x.PullCount, "Pull Count");
            excel.AddMapping<TreatmentsExcel>(x => x.PullDate, "Pull Date");
            excel.AddMapping<TreatmentsExcel>(x => x.RailDate, "Rail Date");
            excel.AddMapping<TreatmentsExcel>(x => x.RailDiagnosis, "Rail Diagnosis");
            excel.AddMapping<TreatmentsExcel>(x => x.RailDiagnosisCode, "Rail Diagnosis Code");
            excel.AddMapping<TreatmentsExcel>(x => x.RailDoctor, "Rail Doctor");
            excel.AddMapping<TreatmentsExcel>(x => x.RailPenRider, "Rail Pen Rider");
            excel.AddMapping<TreatmentsExcel>(x => x.RailWeight, "Rail Weight");
            excel.AddMapping<TreatmentsExcel>(x => x.RealizedDate, "Realized Date");
            excel.AddMapping<TreatmentsExcel>(x => x.RealizedDiagnosis, "Realized Diagnosis");
            excel.AddMapping<TreatmentsExcel>(x => x.RealizedDiagnosisCode, "Realized Diagnosis Code");
            excel.AddMapping<TreatmentsExcel>(x => x.RealizedDoctor, "Realized Doctor");
            excel.AddMapping<TreatmentsExcel>(x => x.RealizedPenRider, "Date In");
            excel.AddMapping<TreatmentsExcel>(x => x.RealizedWeight, "Realized Weight");
            excel.AddMapping<TreatmentsExcel>(x => x.ShipDate, "Ship Date");
            excel.AddMapping<TreatmentsExcel>(x => x.SpecialPen, "Special Pen");
            excel.AddMapping<TreatmentsExcel>(x => x.TreatmentDay, "Treatment Day");


            var data = from x in excel.Worksheet<TreatmentsExcel>("Treatments")
                       select x;

            //Start Creating Database and Tables.
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            CreateDatabase(connectionString);
            Console.WriteLine("Database Created sucessfully");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

            //creating an object of lots
            int k = 0;
            foreach (var i in data)
            {
                
                TreatmentsDB treatments = new TreatmentsDB
                {
                    TreatmentsId = i.Id,
                    Lot = i.Lot,
                    Pen = i.Pen,
                    SpecialPen = i.SpecialPen,
                    Tag = i.Tag,
                    EID = i.EID,
                    FirstHospitalInDate = i.FirstHospitalInDate,
                    LastHospitalOutDate = i.LastHospitalOutDate,
                    CurrentHospitalInDate = i.CurrentHospitalInDate,
                    PullDate = i.PullDate,
                    PullCount = i.PullCount,
                    DiagnosisCode = i.DiagnosisCode,
                    Diagnosis = i.Diagnosis,
                    Regimen = i.Regimen,
                    TreatmentDay = i.TreatmentDay,
                    PenRider = i.PenRider,
                    Doctor = i.Doctor,
                    Status = i.Status,
                    ShipDate = i.ShipDate,
                    Temp = i.Temp,
                    Weight = i.Weight,
                    Severity = i.Severity,
                    LungScore = i.LungScore,
                    ItemCode = i.ItemCode,
                    ItemName = i.ItemName,
                    ItemType = i.ItemType,
                    Dose = i.Dose,
                    UOM = i.UOM,
                    Price = i.Price,
                    DeathDate = i.DeathDate,
                    DeathWeight = i.DeathWeight,
                    DeathDiagnosisCode = i.DeathDiagnosisCode,
                    DeathDiagnosis = i.DeathDiagnosis,
                    DeathPenRider = i.DeathPenRider,
                    DeathDoctor = i.DeathDoctor,
                    RailDate = i.RailDate,
                    RailWeight = i.RailWeight,
                    RailDiagnosisCode = i.RailDiagnosisCode,
                    RailDiagnosis = i.RailDiagnosis,
                    RailPenRider = i.RailPenRider,
                    RailDoctor = i.RailDoctor,
                    RealizedDate = i.RealizedDate,
                    RealizedWeight = i.RealizedWeight,
                    RealizedDiagnosisCode = i.RealizedDiagnosisCode,
                    RealizedDiagnosis = i.RealizedDiagnosis,
                    RealizedPenRider = i.RealizedPenRider,
                    RealizedDoctor = i.RealizedDoctor
                };

                //saving customer in database.
                using (ISession session = _sessionFactory.OpenSession())
                    session.Save(treatments);
                Console.WriteLine("Saved one!" + k);
                k++;
            }

            Console.WriteLine("Treatments Data Saved");
            Console.ReadKey();

        }
        //TODO #1  SQL Saying LotsDBMap is not an object.
        static void CreateDatabase(string connectionString)
        {
            var configuration = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql7.ConnectionString(connectionString))
                .Mappings(m => m.FluentMappings.Add<TreatmentsDBMap>())
                .BuildConfiguration();

            var exporter = new SchemaExport(configuration);
            exporter.Execute(true, true, false);

            _sessionFactory = configuration.BuildSessionFactory();
        }

    }
}

