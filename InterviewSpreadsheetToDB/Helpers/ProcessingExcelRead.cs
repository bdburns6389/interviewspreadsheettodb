﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using InterviewSpreadsheetToDB.Models;
using LinqToExcel;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Helpers
{
    class ProcessingExcelRead
    {
        private static ISessionFactory _sessionFactory;

        public static void ReadExcel()
        {
            var excel = new ExcelQueryFactory
            {
                FileName = @"C:\Users\brian\Desktop\Data_09_21_2018\wtproc.xlsx"
            };
            excel.AddMapping<ProcessingExcel>(x => x.DiagnosisCode, "Diagnosis Code");
            excel.AddMapping<ProcessingExcel>(x => x.HeadCount, "Head Count");
            excel.AddMapping<ProcessingExcel>(x => x.ItemCode, "Item Code");
            excel.AddMapping<ProcessingExcel>(x => x.ItemHead, "Item Head");
            excel.AddMapping<ProcessingExcel>(x => x.ItemName, "Item Name");
            excel.AddMapping<ProcessingExcel>(x => x.OrderType, "Order Type");

            var data = from x in excel.Worksheet<ProcessingExcel>("Processing")
                       select x;

            //Start Creating Database and Tables.
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            CreateDatabase(connectionString);
            Console.WriteLine("Database Created sucessfully");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

            //creating an object of lots
            foreach (var i in data)
            {
                ProcessingDB processing = new ProcessingDB
                {
                    ProcessingId = i.Id,
                    Lot = i.Lot,
                    Pen = i.Pen,
                    Date = i.Date,
                    HeadCount = i.HeadCount,
                    DiagnosisCode = i.DiagnosisCode,
                    Diagnosis = i.Diagnosis,
                    Regimen = i.Regimen,
                    ItemCode = i.ItemCode,
                    ItemName = i.ItemName,
                    ItemHead = i.ItemHead,
                    Dose = i.Dose,
                    UOM = i.UOM,
                    Price = i.Price,
                    OrderType = i.OrderType
                };

                //saving customer in database.
                using (ISession session = _sessionFactory.OpenSession())
                    session.Save(processing);
            }

            Console.WriteLine("Processing Data Saved");
            Console.ReadKey();

        }
        //TODO #1  SQL Saying LotsDBMap is not an object.
        static void CreateDatabase(string connectionString)
        {
            var configuration = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql7.ConnectionString(connectionString))
                .Mappings(m => m.FluentMappings.Add<ProcessingDBMap>())
                .BuildConfiguration();

            var exporter = new SchemaExport(configuration);
            exporter.Execute(true, true, false);

            _sessionFactory = configuration.BuildSessionFactory();
        }
    }
}
