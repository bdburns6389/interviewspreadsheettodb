﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using InterviewSpreadsheetToDB.Models;
using LinqToExcel;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Helpers
{
    class SpecialPensExcelRead
    {
        private static ISessionFactory _sessionFactory;

        public static void ReadExcel()
        {
            var excel = new ExcelQueryFactory
            {
                FileName = @"C:\Users\brian\Desktop\Data_09_21_2018\wtspen.xlsx"
            };


            var data = from x in excel.Worksheet<SpecialPensExcel>("SpecialPens")
                       select x;
            //Start Creating Database and Tables.
            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            CreateDatabase(connectionString);
            Console.WriteLine("Database Created sucessfully");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

            //creating an object of lots
            foreach (var i in data)
            {
                SpecialPensDB specialPens = new SpecialPensDB
                {
                    SpecialPensId = i.Id,
                    Pen = i.Pen,
                    Type = i.Type
                };

                //saving customer in database.
                using (ISession session = _sessionFactory.OpenSession())
                    session.Save(specialPens);
            }

            Console.WriteLine("Sex Codes Data Saved");
            Console.ReadKey();

        }
        //TODO #1  SQL Saying LotsDBMap is not an object.
        static void CreateDatabase(string connectionString)
        {
            var configuration = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql7.ConnectionString(connectionString))
                .Mappings(m => m.FluentMappings.Add<SpecialPensDBMap>())
                .BuildConfiguration();

            var exporter = new SchemaExport(configuration);
            exporter.Execute(true, true, false);

            _sessionFactory = configuration.BuildSessionFactory();
        }
    }
}

