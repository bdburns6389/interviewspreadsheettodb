﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using InterviewSpreadsheetToDB.Models;
using LinqToExcel;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Helpers
{
    class LotsExcelRead
    {
        private static ISessionFactory _sessionFactory;

        public static void ReadExcel()
        {
            var excel = new ExcelQueryFactory
            {
                FileName = @"C:\Users\brian\Desktop\Data_09_21_2018\wtlot.xlsx"
            };
            excel.AddMapping<LotsExcel>(x => x.DateIn, "Date In");
            excel.AddMapping<LotsExcel>(x => x.Dateout, "Date Out");
            excel.AddMapping<LotsExcel>(x => x.FeedDry, "Feed Dry");
            excel.AddMapping<LotsExcel>(x => x.FeedWet, "Feed Wet");
            excel.AddMapping<LotsExcel>(x => x.HeadIn, "Head In");
            excel.AddMapping<LotsExcel>(x => x.HeadOut, "Head Out");
            excel.AddMapping<LotsExcel>(x => x.PayWt, "Pay Wt");
            excel.AddMapping<LotsExcel>(x => x.WeightIn, "Weight In");
            excel.AddMapping<LotsExcel>(x => x.WeightOut, "Weight Out");

            var data = from x in excel.Worksheet<LotsExcel>("Lots")
                       select x;

            string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            CreateDatabase(connectionString);
            Console.WriteLine("Database Created sucessfully");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

            //creating an object of lots
            foreach (var i in data)
            {
                LotsDB lots = new LotsDB
                {
                    LotsId = 1,
                    Lot = i.Lot,
                    Pen = i.Pen,
                    Status = i.Status,
                    HeadIn = i.HeadIn,
                    DateIn = i.DateIn,
                    PayWt = i.PayWt,
                    WeightIn = i.WeightIn,
                    Owners = i.Owners,
                    Sex = i.Sex,
                    Breeds = i.Breeds,
                    Buyers = i.Buyers,
                    Origins = i.Origins,
                    Risk = i.Risk,
                    Deads = i.Deads,
                    Railers = i.Railers,
                    HeadOut = i.HeadOut,
                    Dateout = i.Dateout,
                    WeightOut = i.WeightOut,
                    FeedWet = i.FeedWet,
                    FeedDry = i.FeedDry
                };

                //saving customer in database.
                using (ISession session = _sessionFactory.OpenSession())
                    session.Save(lots);
            }

            Console.WriteLine("Lot Data Saved");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

        }
        //TODO #1  SQL Saying LotsDBMap is not an object.
        static void CreateDatabase(string connectionString)
        {
            var configuration = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql7.ConnectionString(connectionString))
                .Mappings(m => m.FluentMappings.Add<LotsDBMap>())
                .BuildConfiguration();

            var exporter = new SchemaExport(configuration);
            exporter.Execute(true, true, false);

            _sessionFactory = configuration.BuildSessionFactory();
        }
    }
}
