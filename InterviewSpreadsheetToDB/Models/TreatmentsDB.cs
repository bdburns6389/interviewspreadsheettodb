﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Models
{
    class TreatmentsDB
    {
        public virtual int TreatmentsId { get; set; }
        public virtual string Lot { get; set; }
        public virtual string Pen { get; set; }
        public virtual string SpecialPen { get; set; }
        public virtual string Tag { get; set; }
        public virtual string EID { get; set; }
        public virtual string FirstHospitalInDate { get; set; }
        public virtual string LastHospitalOutDate { get; set; }
        public virtual string CurrentHospitalInDate { get; set; }
        public virtual string PullDate { get; set; }
        public virtual string PullCount { get; set; }
        public virtual string DiagnosisCode { get; set; }
        public virtual string Diagnosis { get; set; }
        public virtual string Regimen { get; set; }
        public virtual string TreatmentDay { get; set; }
        public virtual string PenRider { get; set; }
        public virtual string Doctor { get; set; }
        public virtual string Status { get; set; }
        public virtual string ShipDate { get; set; }
        public virtual string Temp { get; set; }
        public virtual string Weight { get; set; }
        public virtual string Severity { get; set; }
        public virtual string LungScore { get; set; }
        public virtual string ItemCode { get; set; }
        public virtual string ItemName { get; set; }
        public virtual string ItemType { get; set; }
        public virtual string Dose { get; set; }
        public virtual string UOM { get; set; }
        public virtual string Price { get; set; }
        public virtual string DeathDate { get; set; }
        public virtual string DeathWeight { get; set; }
        public virtual string DeathDiagnosisCode { get; set; }
        public virtual string DeathDiagnosis { get; set; }
        public virtual string DeathPenRider { get; set; }
        public virtual string DeathDoctor { get; set; }
        public virtual string RailDate { get; set; }
        public virtual string RailWeight { get; set; }
        public virtual string RailDiagnosisCode { get; set; }
        public virtual string RailDiagnosis { get; set; }
        public virtual string RailPenRider { get; set; }
        public virtual string RailDoctor { get; set; }
        public virtual string RealizedDate { get; set; }
        public virtual string RealizedWeight { get; set; }
        public virtual string RealizedDiagnosisCode { get; set; }
        public virtual string RealizedDiagnosis { get; set; }
        public virtual string RealizedPenRider { get; set; }
        public virtual string RealizedDoctor { get; set; }
    }

    class TreatmentsDBMap: ClassMap<TreatmentsDB>
    {
        public TreatmentsDBMap()
        {
            Id(c => c.TreatmentsId);
            Map(c => c.Lot);
            Map(c => c.Pen);
            Map(c => c.SpecialPen);
            Map(c => c.Tag);
            Map(c => c.EID);
            Map(c => c.FirstHospitalInDate);
            Map(c => c.LastHospitalOutDate);
            Map(c => c.CurrentHospitalInDate);
            Map(c => c.PullDate);
            Map(c => c.PullCount);
            Map(c => c.DiagnosisCode);
            Map(c => c.Diagnosis);
            Map(c => c.Regimen);
            Map(c => c.TreatmentDay);
            Map(c => c.PenRider);
            Map(c => c.Doctor);
            Map(c => c.Status);
            Map(c => c.ShipDate);
            Map(c => c.Temp);
            Map(c => c.Weight);
            Map(c => c.Severity);
            Map(c => c.LungScore);
            Map(c => c.ItemCode);
            Map(c => c.ItemName);
            Map(c => c.ItemType);
            Map(c => c.Dose);
            Map(c => c.UOM);
            Map(c => c.Price);
            Map(c => c.DeathDate);
            Map(c => c.DeathWeight);
            Map(c => c.DeathDiagnosisCode);
            Map(c => c.DeathDiagnosis);
            Map(c => c.DeathPenRider);
            Map(c => c.DeathDoctor);
            Map(c => c.RailDate);
            Map(c => c.RailWeight);
            Map(c => c.RailDiagnosisCode);
            Map(c => c.RailDiagnosis);
            Map(c => c.RailPenRider);
            Map(c => c.RailDoctor);
            Map(c => c.RealizedDate);
            Map(c => c.RealizedWeight);
            Map(c => c.RealizedDiagnosisCode);
            Map(c => c.RealizedDiagnosis);
            Map(c => c.RealizedPenRider);
            Map(c => c.RealizedDoctor);
        }
    }
}
