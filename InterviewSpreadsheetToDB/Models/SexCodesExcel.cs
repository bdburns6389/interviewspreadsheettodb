﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Models
{
    class SexCodesExcel
    {
        public int Id { get; set; }
        public string SexCode { get; set; }
        public string Description { get; set; }
    }
}
