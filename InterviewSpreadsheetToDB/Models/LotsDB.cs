﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Models
{
    public class LotsDB
    {
        public virtual int LotsId { get; set; }
        public virtual string Lot { get; set; }
        public virtual string Pen { get; set; }
        public virtual string Status { get; set; }
        public virtual string HeadIn { get; set; }
        public virtual string DateIn { get; set; }
        public virtual string PayWt { get; set; }
        public virtual string WeightIn { get; set; }
        public virtual string Owners { get; set; }
        public virtual string Sex { get; set; }
        public virtual string Breeds { get; set; }
        public virtual string Buyers { get; set; }
        public virtual string Origins { get; set; }
        public virtual string Risk { get; set; }
        public virtual string Deads { get; set; }
        public virtual string Railers { get; set; }
        public virtual string HeadOut { get; set; }
        public virtual string Dateout { get; set; }
        public virtual string WeightOut { get; set; }
        public virtual string FeedWet { get; set; }
        public virtual string FeedDry { get; set; }
    }

    public class LotsDBMap : ClassMap<LotsDB>
    {
        public LotsDBMap()
        {
            Id(c => c.LotsId);
            Map(c => c.Lot);
            Map(c => c.Pen);
            Map(c => c.Status);
            Map(c => c.HeadIn);
            Map(c => c.DateIn);
            Map(c => c.PayWt);
            Map(c => c.WeightIn);
            Map(c => c.Owners);
            Map(c => c.Sex);
            Map(c => c.Breeds);
            Map(c => c.Buyers);
            Map(c => c.Origins);
            Map(c => c.Risk);
            Map(c => c.Deads);
            Map(c => c.Railers);
            Map(c => c.HeadOut);
            Map(c => c.Dateout);
            Map(c => c.WeightOut);
            Map(c => c.FeedWet);
            Map(c => c.FeedDry);
        }
    }
}
