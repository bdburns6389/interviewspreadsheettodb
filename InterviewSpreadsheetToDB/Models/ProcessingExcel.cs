﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Models
{
    class ProcessingExcel
    {
        public int Id { get; set; }
        public string Lot { get; set; }
        public string Pen { get; set; }
        public string Date { get; set; }
        public string HeadCount { get; set; }
        public string DiagnosisCode { get; set; }
        public string Diagnosis { get; set; }
        public string Regimen { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ItemHead { get; set; }
        public string Dose { get; set; }
        public string UOM { get; set; }
        public string Price { get; set; }
        public string OrderType { get; set; }
    }
}
