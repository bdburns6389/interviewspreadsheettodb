﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Models
{
    class ProcessingDB
    {
        public virtual int ProcessingId { get; set; }
        public virtual string Lot { get; set; }
        public virtual string Pen { get; set; }
        public virtual string Date { get; set; }
        public virtual string HeadCount { get; set; }
        public virtual string DiagnosisCode { get; set; }
        public virtual string Diagnosis { get; set; }
        public virtual string Regimen { get; set; }
        public virtual string ItemCode { get; set; }
        public virtual string ItemName { get; set; }
        public virtual string ItemHead { get; set; }
        public virtual string Dose { get; set; }
        public virtual string UOM { get; set; }
        public virtual string Price { get; set; }
        public virtual string OrderType { get; set; }
    }

    class ProcessingDBMap: ClassMap<ProcessingDB>
    {
        public ProcessingDBMap()
        {
            Id(c => c.ProcessingId);
            Map(c => c.Lot);
            Map(c => c.Pen);
            Map(c => c.Date);
            Map(c => c.HeadCount);
            Map(c => c.DiagnosisCode);
            Map(c => c.Diagnosis);
            Map(c => c.Regimen);
            Map(c => c.ItemCode);
            Map(c => c.ItemName);
            Map(c => c.ItemHead);
            Map(c => c.Dose);
            Map(c => c.UOM);
            Map(c => c.Price);
            Map(c => c.OrderType);
        }
    }
}
