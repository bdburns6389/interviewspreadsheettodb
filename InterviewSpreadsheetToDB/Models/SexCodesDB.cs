﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Models
{
    class SexCodesDB
    {
        public virtual int SexCodesId { get; set; }
        public virtual string SexCode { get; set; }
        public virtual string Description { get; set; }
    }

    class SexCodesDBMap : ClassMap<SexCodesDB>
    {
        public SexCodesDBMap()
        {
            Id(c => c.SexCodesId);
            Map(c => c.SexCode);
            Map(c => c.Description);
        }
    }
}
