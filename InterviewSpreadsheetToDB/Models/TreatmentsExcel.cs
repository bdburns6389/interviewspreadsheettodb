﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Models
{
    class TreatmentsExcel
    {
        public int Id { get; set; }
        public string Lot { get; set; }
        public string Pen { get; set; }
        public string SpecialPen { get; set; }
        public string Tag { get; set; }
        public string EID { get; set; }
        public string FirstHospitalInDate { get; set; }
        public string LastHospitalOutDate { get; set; }
        public string CurrentHospitalInDate { get; set; }
        public string PullDate { get; set; }
        public string PullCount { get; set; }
        public string DiagnosisCode { get; set; }
        public string Diagnosis { get; set; }
        public string Regimen { get; set; }
        public string TreatmentDay { get; set; }
        public string PenRider { get; set; }
        public string Doctor { get; set; }
        public string Status { get; set; }
        public string ShipDate { get; set; }
        public string Temp { get; set; }
        public string Weight { get; set; }
        public string Severity { get; set; }
        public string LungScore { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ItemType { get; set; }
        public string Dose { get; set; }
        public string UOM { get; set; }
        public string Price { get; set; }
        public string DeathDate { get; set; }
        public string DeathWeight { get; set; }
        public string DeathDiagnosisCode { get; set; }
        public string DeathDiagnosis { get; set; }
        public string DeathPenRider { get; set; }
        public string DeathDoctor { get; set; }
        public string RailDate { get; set; }
        public string RailWeight { get; set; }
        public string RailDiagnosisCode { get; set; }
        public string RailDiagnosis { get; set; }
        public string RailPenRider { get; set; }
        public string RailDoctor { get; set; }
        public string RealizedDate { get; set; }
        public string RealizedWeight { get; set; }
        public string RealizedDiagnosisCode { get; set; }
        public string RealizedDiagnosis { get; set; }
        public string RealizedPenRider { get; set; }
        public string RealizedDoctor { get; set; }

    }
}

