﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Models
{
    class LotsExcel
    {
        public int Id { get; set; }
        public string Lot { get; set; }
        public string Pen { get; set; }
        public string Status { get; set; }
        public string HeadIn { get; set; }
        public string DateIn { get; set; }
        public string PayWt { get; set; }
        public string WeightIn { get; set; }
        public string Owners { get; set; }
        public string Sex { get; set; }
        public string Breeds { get; set; }
        public string Buyers { get; set; }
        public string Origins { get; set; }
        public string Risk { get; set; }
        public string Deads { get; set; }
        public string Railers { get; set; }
        public string HeadOut { get; set; }
        public string Dateout { get; set; }
        public string WeightOut { get; set; }
        public string FeedWet { get; set; }
        public string FeedDry { get; set; }



    }
}
