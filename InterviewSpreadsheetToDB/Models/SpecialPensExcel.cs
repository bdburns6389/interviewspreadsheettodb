﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Models
{
    class SpecialPensExcel
    {
        public int Id { get; set; }
        public string Pen { get; set; }
        public string Type { get; set; }

    }
}
