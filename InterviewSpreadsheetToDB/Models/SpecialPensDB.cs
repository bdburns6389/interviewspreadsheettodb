﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewSpreadsheetToDB.Models
{
    class SpecialPensDB
    {
        public virtual int SpecialPensId { get; set; }
        public virtual string Pen { get; set; }
        public virtual string Type { get; set; }
    }

    class SpecialPensDBMap: ClassMap<SpecialPensDB>
    {
        public SpecialPensDBMap()
        {
            Id(c => c.SpecialPensId);
            Map(c => c.Pen);
            Map(c => c.Type);
        }
    }
}
